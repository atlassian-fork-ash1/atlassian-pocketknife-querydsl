package com.atlassian.pocketknife.test.util.querydsl;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnectionConverter;
import com.atlassian.pocketknife.api.querydsl.EitherAwareDatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.OptionalAwareDatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.configuration.ConfigurationEnrichment;
import com.atlassian.pocketknife.api.querydsl.schema.DialectProvider;
import com.atlassian.pocketknife.api.querydsl.schema.SchemaStateProvider;
import com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory;

/**
 * Some times during testing your need access to the underlying API components that are wired
 * together during tests.  In production this would come from the Spring runtime but in a standalone
 * test we need a different mechanism to get the wired runtime
 *
 * @since v4.1.0
 */
public interface StandaloneApiRuntime {

    /**
     * @return the dialect provider in play
     */
    DialectProvider getDialectProvider();

    /**
     * @return the connection converter in play
     */
    DatabaseConnectionConverter getDatabaseConnectionConverter();

    /**
     * @return the streaming query factory in play
     */
    StreamingQueryFactory getStreamingQueryFactory();

    /**
     * @return the schema state provider in play
     */
    SchemaStateProvider getSchemaStateProvider();

    /**
     * @return the configuration enrichment in play
     */
    ConfigurationEnrichment getConfigurationEnrichment();

    /**
     * @return the database accessor in play
     */
    DatabaseAccessor getDatabaseAccessor();

    /**
     * @return the database accessor in play
     */
    OptionalAwareDatabaseAccessor getOptionalAwareDatabaseAccessor();

    /**
     * @return the database accessor in play
     */
    EitherAwareDatabaseAccessor getEitherAwareDatabaseAccessor();
}
