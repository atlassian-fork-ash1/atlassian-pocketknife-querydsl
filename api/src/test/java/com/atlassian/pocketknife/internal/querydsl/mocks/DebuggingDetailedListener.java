package com.atlassian.pocketknife.internal.querydsl.mocks;

import com.atlassian.pocketknife.internal.querydsl.listener.AbstractDetailedLoggingListener;
import com.querydsl.sql.SQLListenerContext;

import java.util.concurrent.atomic.AtomicReference;

public class DebuggingDetailedListener extends AbstractDetailedLoggingListener {

    AtomicReference<String> sql = new AtomicReference<>();

    public String getLastSQL() {
        return sql.get();
    }

    @Override
    public void start(SQLListenerContext context) {
        sql.set(null);
    }

    @Override
    public void rendered(SQLListenerContext context) {
        sql.set(context.getSQL());
    }

    @Override
    public void executed(SQLListenerContext context) {
        sql.set(context.getSQL());
    }

    @Override
    public void end(SQLListenerContext context) {
        sql.set(context.getSQL());
    }
}
