package com.atlassian.pocketknife.internal.querydsl.tables.domain;

import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.core.types.dsl.TimePath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.ForeignKey;
import com.querydsl.sql.PrimaryKey;
import com.querydsl.sql.RelationalPathBase;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;

public class QEmployeeWrongCase extends RelationalPathBase<Employee> {

    public static final QEmployeeWrongCase employee = new QEmployeeWrongCase("emPloYEE");

    private static final long serialVersionUID = 1394463749655231079L;

    public final NumberPath<Integer> id = createNumber("iD", Integer.class);

    public final StringPath firstname = createString("firstName");

    public final StringPath lastname = createString("lastName");

    public final NumberPath<BigDecimal> salary = createNumber("salary", BigDecimal.class);

    public final DatePath<Date> datefield = createDate("dateField", Date.class);

    public final TimePath<Time> timefield = createTime("timeField", Time.class);

    public final NumberPath<Integer> superiorId = createNumber("superior_Id", Integer.class);

    public final PrimaryKey<Employee> idKey = createPrimaryKey(id);

    public final ForeignKey<Employee> superiorIdKey = createForeignKey(superiorId, "iD");

    public final ForeignKey<Employee> _superiorIdKey = createInvForeignKey(id, "superior_Id");

    public QEmployeeWrongCase(String path) {
        super(Employee.class, PathMetadataFactory.forVariable(path), "publiC", "emPloYEE");
        addMetadata();
    }

    protected void addMetadata() {
        addMetadata(id, ColumnMetadata.named("iD"));
        addMetadata(firstname, ColumnMetadata.named("firstName"));
        addMetadata(lastname, ColumnMetadata.named("lastName"));
        addMetadata(salary, ColumnMetadata.named("salary"));
        addMetadata(datefield, ColumnMetadata.named("dateField"));
        addMetadata(timefield, ColumnMetadata.named("timeField"));
        addMetadata(superiorId, ColumnMetadata.named("superiorId"));
    }

}
