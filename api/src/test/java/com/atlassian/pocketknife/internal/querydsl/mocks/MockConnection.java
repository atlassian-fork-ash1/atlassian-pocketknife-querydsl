package com.atlassian.pocketknife.internal.querydsl.mocks;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

public class MockConnection implements Connection
{
    private final Connection delegate;


    public MockConnection(final Connection delegate)
    {
        this.delegate = delegate;
    }

    @Override
    public void abort(final Executor executor) throws SQLException
    {
        delegate.abort(executor);
    }

    @Override
    public void clearWarnings() throws SQLException
    {
        delegate.clearWarnings();
    }

    @Override
    public void close() throws SQLException
    {
        delegate.close();
    }

    @Override
    public void commit() throws SQLException
    {
        delegate.commit();
    }

    @Override
    public Array createArrayOf(final String typeName, final Object[] elements) throws SQLException
    {
        return delegate.createArrayOf(typeName, elements);
    }

    @Override
    public Blob createBlob() throws SQLException
    {
        return delegate.createBlob();
    }

    @Override
    public Clob createClob() throws SQLException
    {
        return delegate.createClob();
    }

    @Override
    public NClob createNClob() throws SQLException
    {
        return delegate.createNClob();
    }

    @Override
    public SQLXML createSQLXML() throws SQLException
    {
        return delegate.createSQLXML();
    }

    @Override
    public Statement createStatement() throws SQLException
    {
        return delegate.createStatement();
    }

    @Override
    public Statement createStatement(final int resultSetType, final int resultSetConcurrency) throws SQLException
    {
        return delegate.createStatement(resultSetType, resultSetConcurrency);
    }

    @Override
    public Statement createStatement(final int resultSetType, final int resultSetConcurrency, final int resultSetHoldability)
            throws SQLException
    {
        return delegate.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    @Override
    public Struct createStruct(final String typeName, final Object[] attributes) throws SQLException
    {
        return delegate.createStruct(typeName, attributes);
    }

    @Override
    public boolean getAutoCommit() throws SQLException
    {
        return delegate.getAutoCommit();
    }

    @Override
    public String getCatalog() throws SQLException
    {
        return delegate.getCatalog();
    }

    @Override
    public Properties getClientInfo() throws SQLException
    {
        return delegate.getClientInfo();
    }

    @Override
    public String getClientInfo(final String name) throws SQLException
    {
        return delegate.getClientInfo(name);
    }

    @Override
    public int getHoldability() throws SQLException
    {
        return delegate.getHoldability();
    }

    @Override
    public DatabaseMetaData getMetaData() throws SQLException
    {
        return delegate.getMetaData();
    }

    @Override
    public int getNetworkTimeout() throws SQLException
    {
        return delegate.getNetworkTimeout();
    }

    @Override
    public String getSchema() throws SQLException
    {
        return delegate.getSchema();
    }

    @Override
    public int getTransactionIsolation() throws SQLException
    {
        return delegate.getTransactionIsolation();
    }

    @Override
    public Map<String, Class<?>> getTypeMap() throws SQLException
    {
        return delegate.getTypeMap();
    }

    @Override
    public SQLWarning getWarnings() throws SQLException
    {
        return delegate.getWarnings();
    }

    @Override
    public boolean isClosed() throws SQLException
    {
        return delegate.isClosed();
    }

    @Override
    public boolean isReadOnly() throws SQLException
    {
        return delegate.isReadOnly();
    }

    @Override
    public boolean isValid(final int timeout) throws SQLException
    {
        return delegate.isValid(timeout);
    }

    @Override
    public String nativeSQL(final String sql) throws SQLException
    {
        return delegate.nativeSQL(sql);
    }

    @Override
    public CallableStatement prepareCall(final String sql) throws SQLException
    {
        return delegate.prepareCall(sql);
    }

    @Override
    public CallableStatement prepareCall(final String sql, final int resultSetType, final int resultSetConcurrency)
            throws SQLException
    {
        return delegate.prepareCall(sql, resultSetType, resultSetConcurrency);
    }

    @Override
    public CallableStatement prepareCall(final String sql, final int resultSetType, final int resultSetConcurrency, final int resultSetHoldability)
            throws SQLException
    {
        return delegate.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    @Override
    public PreparedStatement prepareStatement(final String sql) throws SQLException
    {
        return delegate.prepareStatement(sql);
    }

    @Override
    public PreparedStatement prepareStatement(final String sql, final int autoGeneratedKeys) throws SQLException
    {
        return delegate.prepareStatement(sql, autoGeneratedKeys);
    }

    @Override
    public PreparedStatement prepareStatement(final String sql, final int[] columnIndexes) throws SQLException
    {
        return delegate.prepareStatement(sql, columnIndexes);
    }

    @Override
    public PreparedStatement prepareStatement(final String sql, final String[] columnNames) throws SQLException
    {
        return delegate.prepareStatement(sql, columnNames);
    }

    @Override
    public PreparedStatement prepareStatement(final String sql, final int resultSetType, final int resultSetConcurrency)
            throws SQLException
    {
        return delegate.prepareStatement(sql, resultSetType, resultSetConcurrency);
    }

    @Override
    public PreparedStatement prepareStatement(final String sql, final int resultSetType, final int resultSetConcurrency, final int resultSetHoldability)
            throws SQLException
    {
        return delegate.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    @Override
    public void releaseSavepoint(final Savepoint savepoint) throws SQLException
    {
        delegate.releaseSavepoint(savepoint);
    }

    @Override
    public void rollback() throws SQLException
    {
        delegate.rollback();
    }

    @Override
    public void rollback(final Savepoint savepoint) throws SQLException
    {
        delegate.rollback(savepoint);
    }

    @Override
    public void setAutoCommit(final boolean autoCommit) throws SQLException
    {
        delegate.setAutoCommit(autoCommit);
    }

    @Override
    public void setCatalog(final String catalog) throws SQLException
    {
        delegate.setCatalog(catalog);
    }

    @Override
    public void setClientInfo(final String name, final String value) throws SQLClientInfoException
    {
        delegate.setClientInfo(name, value);
    }

    @Override
    public void setClientInfo(final Properties properties) throws SQLClientInfoException
    {
        delegate.setClientInfo(properties);
    }

    @Override
    public void setHoldability(final int holdability) throws SQLException
    {
        delegate.setHoldability(holdability);
    }

    @Override
    public void setNetworkTimeout(final Executor executor, final int milliseconds) throws SQLException
    {
        delegate.setNetworkTimeout(executor, milliseconds);
    }

    @Override
    public void setReadOnly(final boolean readOnly) throws SQLException
    {
        delegate.setReadOnly(readOnly);
    }

    @Override
    public Savepoint setSavepoint() throws SQLException
    {
        return delegate.setSavepoint();
    }

    @Override
    public Savepoint setSavepoint(final String name) throws SQLException
    {
        return delegate.setSavepoint(name);
    }

    @Override
    public void setSchema(final String schema) throws SQLException
    {
        delegate.setSchema(schema);
    }

    @Override
    public void setTransactionIsolation(final int level) throws SQLException
    {
        delegate.setTransactionIsolation(level);
    }

    @Override
    public void setTypeMap(final Map<String, Class<?>> map) throws SQLException
    {
        delegate.setTypeMap(map);
    }

    @Override
    public boolean isWrapperFor(final Class<?> iface) throws SQLException
    {
        return delegate.isWrapperFor(iface);
    }

    @Override
    public <T> T unwrap(final Class<T> iface) throws SQLException
    {
        return delegate.unwrap(iface);
    }
}
