package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.pocketknife.api.querydsl.stream.ClosePromise;
import com.atlassian.pocketknife.api.querydsl.stream.CloseableIterable;
import com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory;
import com.atlassian.pocketknife.internal.querydsl.mocks.TestingContext;
import com.atlassian.pocketknife.internal.querydsl.stream.StreamingQueryFactoryImpl;
import com.atlassian.pocketknife.internal.querydsl.tables.domain.Employee;
import com.atlassian.pocketknife.internal.querydsl.tables.domain.QAOEmployee;
import com.atlassian.pocketknife.internal.querydsl.tables.domain.QEmployeeWrongCase;
import com.atlassian.pocketknife.internal.querydsl.util.Unit;
import com.atlassian.pocketknife.internal.querydsl.util.fp.Fp;
import com.querydsl.core.Tuple;
import com.querydsl.sql.SQLQuery;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import static com.atlassian.pocketknife.api.querydsl.tuple.Tuples.tupleOf;
import static com.atlassian.pocketknife.api.querydsl.util.OnRollback.NOOP;
import static com.atlassian.pocketknife.internal.querydsl.tables.domain.QEmployee.employee;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Tests the basic querying that happens
 */
public class QueryingTest {
    private TestingContext testingContext;
    private DatabaseAccessor databaseAccessor;
    private StreamingQueryFactory streamingQueryFactory;

    @Before
    public void setUp() throws Exception {

        testingContext = TestingContext.forH2();

        databaseAccessor = testingContext.getDatabaseAccessor();
        streamingQueryFactory = new StreamingQueryFactoryImpl();
    }

    @Test
    public void testBasicConnection() throws Exception {
        List<Tuple> data = databaseAccessor.runInNewTransaction(dbConn -> dbConn.select(employee.firstname, employee.lastname).from(employee).fetch(), NOOP);
        assertThat(data.size(), equalTo(10));

    }

    @Test
    public void testFromSelectReorder() throws Exception {
        List<Tuple> data;

        data = databaseAccessor.runInNewTransaction(dbConn -> dbConn.select(employee.firstname, employee.lastname).from(employee).fetch(), NOOP);
        assertThat(data.size(), equalTo(10));

        data = databaseAccessor.runInNewTransaction(dbConn -> dbConn.query().from(employee).select(employee.firstname, employee.lastname).fetch(), NOOP);
        assertThat(data.size(), equalTo(10));

        data = databaseAccessor.runInNewTransaction(dbConn -> dbConn.from(employee).select(employee.firstname, employee.lastname).fetch(), NOOP);
        assertThat(data.size(), equalTo(10));

        List<Employee> employees = databaseAccessor.runInNewTransaction(dbConn -> dbConn.from(employee).fetch(), NOOP);
        assertThat(employees.size(), equalTo(10));
    }


    @Test
    public void test_case_insensitive_table_name_support_querying() throws Exception {
        //
        // if we use a table where the case of the table and columns are wrong then
        // the runtime translation layer via SchemaProvider should fix this up!
        //
        QEmployeeWrongCase WRONGCASE_EMPLOYEE = QEmployeeWrongCase.employee;

        List<Tuple> data;

        data = databaseAccessor.runInNewTransaction(dbConn -> dbConn.
                select(WRONGCASE_EMPLOYEE.firstname, WRONGCASE_EMPLOYEE.lastname)
                .from(WRONGCASE_EMPLOYEE)
                .fetch(), NOOP);

        assertThat(data.size(), equalTo(10));
        //
        // note the pathological table alias (which can be any value) and the schema
        //
        assertThat(testingContext.getLastSQL(), equalTo("select \"emPloYEE\".\"FIRSTNAME\", \"emPloYEE\".\"LASTNAME\" from \"PUBLIC\".\"EMPLOYEE\" \"emPloYEE\""));
    }

    @Test
    public void test_ao_table_name_support_querying() throws Exception {
        //
        // if we use a table where the case of the table and columns are wrong then
        // the runtime translation layer via SchemaProvider should fix this up!
        //
        QAOEmployee AO_EMPLOYEE = QAOEmployee.employee;

        List<Tuple> data;

        data = databaseAccessor.runInNewTransaction(dbConn -> dbConn.
                select(AO_EMPLOYEE.firstname, AO_EMPLOYEE.lastname)
                .from(AO_EMPLOYEE)
                .fetch(), NOOP);

        assertThat(data.size(), equalTo(10));
        //
        // note the pathological table alias (which can be any value) and the schema in play
        //
        assertThat(testingContext.getLastSQL(), equalTo("select \"AO_Employee\".\"FIRSTNAME\", \"AO_Employee\".\"LASTNAME\" from \"PUBLIC\".\"AO_EMPLOYEE\" \"AO_Employee\""));
    }

    @Test
    public void test_with_no_schema_querying() throws Exception {
        List<Tuple> data;


        testingContext = TestingContext
                .builder()
                .withNoSchema()
                .build();

        databaseAccessor = testingContext.getDatabaseAccessor();

        data = databaseAccessor.runInNewTransaction(dbConn -> dbConn.
                select(employee.firstname, employee.lastname)
                .from(employee)
                .fetch(), NOOP);

        assertThat(data.size(), equalTo(10));

        //
        // Note the lack of schema
        //
        assertThat(testingContext.getLastSQL(), equalTo("select \"EMPLOYEE\".\"FIRSTNAME\", \"EMPLOYEE\".\"LASTNAME\" from \"EMPLOYEE\" \"EMPLOYEE\""));
    }

    @Test
    public void test_TupleOf() throws Exception {
        List<Tuple> tuples = databaseAccessor.runInNewTransaction(dbConn -> dbConn.select(tupleOf(employee.firstname)).from(employee).fetch(), NOOP);
        assertThat(tuples.size(), equalTo(10));

        // vs

        List<String> strings = databaseAccessor.runInNewTransaction(dbConn -> dbConn.select(employee.firstname).from(employee).fetch(), NOOP);
        assertThat(strings.size(), equalTo(10));
    }

    @Test
    public void testSelectWithMapping() throws Exception {
        databaseAccessor.runInNewTransaction(dbConn -> {
            final List<Employee> employees = new ArrayList<>();
            try (CloseableIterable<Tuple> streamyQuery = createStreamyQuery(dbConn)) {
                for (Employee e : streamyQuery.map(input -> {
                    Employee e1 = new Employee(input.get(employee.id));
                    e1.setFirstname(input.get(employee.firstname));
                    e1.setLastname(input.get(employee.lastname));
                    return e1;
                })) {
                    employees.add(e);
                }
            }

            assertThat(employees.size(), equalTo(2));
            return Unit.VALUE;
        }, NOOP);

    }

    @Test
    public void testStreamyMap() throws Exception {
        final List<Tuple> queryResult = databaseAccessor.runInNewTransaction(connection -> connection.select(employee.id, employee.firstname).from(employee).fetch(), NOOP);
        final Tuple firstTuple = queryResult.get(0);
        Integer firstEmployeeId = firstTuple.get(employee.id);
        String firstEmployeeName = firstTuple.get(employee.firstname);

        databaseAccessor.runInNewTransaction(dbConn -> {
            TestStreamyMapClosure closure = new TestStreamyMapClosure(firstEmployeeId);
            List<String> result = streamingQueryFactory.streamyMap(dbConn, closure);
            assertThat(result, Matchers.containsInAnyOrder(firstEmployeeName));
            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testSelectWithFold() throws Exception {
        long numberOfEmployees = databaseAccessor.runInNewTransaction(connection -> connection.select(employee.count()).from(employee).fetchCount(), NOOP);

        databaseAccessor.runInNewTransaction(dbConn -> {
            TestStreamyFoldClosure closure = new TestStreamyFoldClosure();

            Supplier<SQLQuery<Integer>> query = Fp.asSupplier(dbConn, closure.getQuery());
            try (CloseableIterable<Integer> allEmployeesStreamy = streamingQueryFactory.stream(dbConn, query)) {
                long streamyCount = allEmployeesStreamy.foldLeft(0, closure.getFoldFunction());

                assertThat(streamyCount, equalTo(numberOfEmployees));
            }
            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testStreamyFold() throws Exception {
        long numberOfEmployees = databaseAccessor.runInNewTransaction(connection -> connection.select(employee.count()).from(employee).fetchCount(), NOOP);

        databaseAccessor.runInNewTransaction(conn -> {
            TestStreamyFoldClosure closure = new TestStreamyFoldClosure();
            long result = streamingQueryFactory.streamyFold(conn, 0, closure);
            assertThat(result, equalTo(numberOfEmployees));
            return Unit.VALUE;
        }, NOOP);
    }

    @Test(expected = RuntimeException.class)
    public void testSelectWhereMappingThrowsException() throws Exception {
        databaseAccessor.runInNewTransaction(dbConn -> {

            final List<Employee> employees = new ArrayList<>();

            try (CloseableIterable<Tuple> streamyQuery = createStreamyQuery(dbConn)) {
                for (Employee e : streamyQuery.map(input -> {
                    //noinspection ConstantConditions,ConstantIfStatement
                    if (true) {
                        throw new RuntimeException("Badness!");
                    }
                    return (Employee) null;
                })) {
                    employees.add(e);
                }
            }
            return employees;
        }, NOOP);
    }

    @Test
    public void testSelectIsNotClosedWhenMappingIsHalfUsed() throws Exception {
        databaseAccessor.runInNewTransaction(dbConn -> {
            try (CloseableIterable<Tuple> streamyQuery = createStreamyQuery(dbConn)) {
                int i = 0;
                for (Employee ignored : streamyQuery.map(input -> {
                    Employee e = new Employee(input.get(employee.id));
                    e.setFirstname(input.get(employee.firstname));
                    e.setLastname(input.get(employee.lastname));
                    return e;
                })) {
                    i++;
                    if (i > 0) {
                        break;
                    }
                }
            }
            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testSelectIsNotClosedWhenIteratorIsHalfUsed() throws Exception {
        databaseAccessor.runInNewTransaction(dbConn -> {
            Supplier<SQLQuery<Tuple>> query = () -> dbConn.select(employee.id, employee.firstname, employee.lastname).from(employee).where(employee.lastname.contains("Smith"));
            try (CloseableIterable<Tuple> streamyResult = streamingQueryFactory.stream(dbConn, query)) {
                int i = 0;
                for (Tuple ignored : streamyResult) {
                    i++;
                    if (i > 0) {
                        break;
                    }
                }
            }
            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testClosePromiseIsCalledWhenResultIsClosed() throws Exception {
        final AtomicBoolean closePromiseRan = new AtomicBoolean(false);
        ClosePromise closePromise = new ClosePromise(() -> closePromiseRan.set(true));

        databaseAccessor.runInNewTransaction(dbConn -> {

            Supplier<SQLQuery<Tuple>> query = () -> dbConn.select(employee.id, employee.firstname, employee.lastname).from(employee).where(employee.lastname.contains("Smith"));
            try (CloseableIterable<Tuple> streamyResult = streamingQueryFactory.stream(dbConn, closePromise, query)) {
                streamyResult.foreach(ignored -> {
                });
            }

            assertThat(closePromiseRan.get(), equalTo(true));
            return Unit.VALUE;
        }, NOOP);

    }

    private CloseableIterable<Tuple> createStreamyQuery(DatabaseConnection dbConn) {
        return streamingQueryFactory.stream(dbConn, () -> dbConn.select(employee.id, employee.firstname, employee.lastname).from(employee).where(employee.lastname.contains("Smith")));
    }

    private class TestStreamyMapClosure implements StreamingQueryFactory.StreamyMapClosure<String, String> {
        private final Integer employeeId;

        private TestStreamyMapClosure(final Integer employeeId) {
            this.employeeId = employeeId;
        }

        @Override
        public Function<DatabaseConnection, SQLQuery<String>> getQuery() {
            return conn -> conn.select(employee.firstname).from(employee).where(employee.id.eq(employeeId));
        }

        @Override
        public Function<String, String> getMapFunction() {
            return input -> input;
        }
    }

    private class TestStreamyFoldClosure implements StreamingQueryFactory.StreamyFoldClosure<Integer, Integer> {
        @Override
        public Function<DatabaseConnection, SQLQuery<Integer>> getQuery() {
            return input -> input.select(employee.id).from(employee);
        }

        @Override
        public BiFunction<Integer, Integer, Integer> getFoldFunction() {
            return (arg0, arg1) -> arg0 + 1;
        }
    }

}

