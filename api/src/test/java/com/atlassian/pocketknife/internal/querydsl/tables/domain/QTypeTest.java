package com.atlassian.pocketknife.internal.querydsl.tables.domain;

import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.RelationalPathBase;

public class QTypeTest extends RelationalPathBase<QTypeTest>
{

    public enum SimpleAnimals {
        RATS, CATS, ELEPHANTS
    }


    public enum ComplexAnimals {
        GREEN_ALLIGATORS, LONG_NECK_GEESE, HUMPY_BACK_CAMELS
    }

    public static final QTypeTest typeTest = new QTypeTest("TYPE_TEST");

    public final EnumPath<SimpleAnimals> t1 = createEnum("t1",SimpleAnimals.class);
    public final EnumPath<ComplexAnimals> t2 = createEnum("t2",ComplexAnimals.class);

    public QTypeTest(String path) {
        super(QTypeTest.class, PathMetadataFactory.forVariable(path), "PUBLIC", path);
        addMetadata();
    }

    protected void addMetadata() {
        addMetadata(t1, ColumnMetadata.named("T1"));
        addMetadata(t2, ColumnMetadata.named("T2"));
    }

}
