package com.atlassian.pocketknife.internal.querydsl.schema;

import com.atlassian.pocketknife.api.querydsl.schema.SchemaState;
import com.atlassian.pocketknife.internal.querydsl.mocks.MockTransactionExecutorFactory;
import com.atlassian.pocketknife.internal.querydsl.tables.TestConnections;
import com.atlassian.pocketknife.internal.querydsl.tables.domain.Employee;
import com.atlassian.pocketknife.internal.querydsl.tables.domain.QEmployee;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.core.types.dsl.TimePath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.RelationalPathBase;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Time;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;

public class SchemaStateProviderImplTest {

    private Connection connection;
    private SchemaStateProviderImpl classUnderTest;

    @Before
    public void setup() throws Exception {
        TestConnections.initHSQL();
        connection = TestConnections.getConnection();

        MockTransactionExecutorFactory executorFactory = new MockTransactionExecutorFactory();
        ProductSchemaProvider productSchemaProvider = new ProductSchemaProvider(executorFactory);
        classUnderTest = new SchemaStateProviderImpl(productSchemaProvider, new JdbcTableInspector());
    }


    @Test
    public void test_schema_state_all_the_same() throws Exception {

        QEmployee EMPLOYEE = QEmployee.employee;

        SchemaState schemaState = classUnderTest.getSchemaState(connection, EMPLOYEE);

        assertThat(schemaState.getRelationalPath(), equalTo(EMPLOYEE));
        assertThat(schemaState.getTableState(), equalTo(SchemaState.Presence.SAME));
        assertThat(schemaState.getMissingColumns().size(), equalTo(0));
        assertThat(schemaState.getAddedColumns().size(), equalTo(0));

        EMPLOYEE.getColumns().forEach(col -> assertThat(schemaState.getColumnState(col), equalTo(SchemaState.Presence.SAME)));
    }

    @Test
    public void test_schema_state_different_table_view() throws Exception {

        // map a different Q object onto the existing table
        QEmployeeDifferent DIFFERENT_EMPLOYEE = new QEmployeeDifferent("EMPLOYEE");

        SchemaState schemaState = classUnderTest.getSchemaState(connection, DIFFERENT_EMPLOYEE);

        assertThat(schemaState.getRelationalPath(), equalTo(DIFFERENT_EMPLOYEE));
        assertThat(schemaState.getTableState(), equalTo(SchemaState.Presence.DIFFERENT));
        assertThat(schemaState.getMissingColumns().size(), equalTo(1));
        assertThat(schemaState.getColumnState(DIFFERENT_EMPLOYEE.logicallyAddedColumn), equalTo(SchemaState.Presence.MISSING));

        assertThat(schemaState.getColumnState(DIFFERENT_EMPLOYEE.id), equalTo(SchemaState.Presence.SAME));
        assertThat(schemaState.getColumnState(DIFFERENT_EMPLOYEE.firstname), equalTo(SchemaState.Presence.SAME));
        assertThat(schemaState.getColumnState(DIFFERENT_EMPLOYEE.lastname), equalTo(SchemaState.Presence.SAME));

        // things added in the physical table but not in the logical definition
        assertThat(schemaState.getAddedColumns().size(), equalTo(1));
        assertThat(schemaState.getAddedColumns(), hasItem("SUPERIOR_ID"));
    }

    @Test
    public void test_schema_state_missing_table() throws Exception {

        QEmployee MISSING_TABLE = new QEmployee("MISSING_TABLE", "MISSING_TABLE");

        SchemaState schemaState = classUnderTest.getSchemaState(connection, MISSING_TABLE);

        assertThat(schemaState.getRelationalPath(), equalTo(MISSING_TABLE));
        assertThat(schemaState.getTableState(), equalTo(SchemaState.Presence.MISSING));
        assertThat(schemaState.getMissingColumns().size(), equalTo(MISSING_TABLE.getColumns().size()));

        MISSING_TABLE.getColumns().forEach(col -> assertThat(schemaState.getColumnState(col), equalTo(SchemaState.Presence.MISSING)));
    }

    class QEmployeeDifferent extends RelationalPathBase<Employee> {

        private static final long serialVersionUID = 1394463749655231079L;

        public final NumberPath<Integer> id = createNumber("id", Integer.class);

        public final StringPath firstname = createString("firstname");

        public final StringPath lastname = createString("lastname");

        public final NumberPath<BigDecimal> salary = createNumber("salary", BigDecimal.class);

        public final DatePath<Date> datefield = createDate("datefield", Date.class);

        public final TimePath<Time> timefield = createTime("timefield", Time.class);

        // missing this physically
        //public final NumberPath<Integer> superiorId = createNumber("superior_Id", Integer.class);

        public final NumberPath<Integer> logicallyAddedColumn = createNumber("logicallyAddedColumn", Integer.class);

        public QEmployeeDifferent(String path) {
            super(Employee.class, PathMetadataFactory.forVariable(path), "PUBLIC", "EMPLOYEE");
            addMetadata();
        }

        @SuppressWarnings("Duplicates")
        protected void addMetadata() {
            addMetadata(id, ColumnMetadata.named("ID"));
            addMetadata(firstname, ColumnMetadata.named("FIRSTNAME"));
            addMetadata(lastname, ColumnMetadata.named("LASTNAME"));
            addMetadata(salary, ColumnMetadata.named("SALARY"));
            addMetadata(datefield, ColumnMetadata.named("DATEFIELD"));
            addMetadata(timefield, ColumnMetadata.named("TIMEFIELD"));
            addMetadata(logicallyAddedColumn, ColumnMetadata.named("LOGICALLY_ADDED_COLUMN"));
        }

    }
}