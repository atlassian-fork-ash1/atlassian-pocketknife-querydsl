package com.atlassian.pocketknife.internal.querydsl.tables.domain;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.core.types.dsl.TimePath;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;

/**
 * An example entity definition based on our extended base
 */
public class QEmailSettings extends EnhancedRelationalPathBase<QEmailSettings> {
    public final NumberPath<Integer> ID = createIntegerCol("ID").asPrimaryKey().build();

    public final DateTimePath CREATED = createDateTimeCol("CREATED", Timestamp.class).notNull().build();

    public final StringPath DESCRIPTION = createStringCol("DESCRIPTION").notNull().build();

    public final StringPath EMAIL_ADDRESS = createString("EMAIL_ADDRESS");

    public final BooleanPath ENABLED = createBoolean("ENABLED");

    public final NumberPath<Long> JIRA_MAIL_SERVER_ID = createLong("JIRA_MAIL_SERVER_ID");

    public final NumberPath<Long> LAST_PROCEEDED_TIME = createLong("LAST_PROCEEDED_TIME");

    public final BooleanPath ON_DEMAND = createBooleanCol("ON_DEMAND").notNull().build();

    public final NumberPath<Integer> REQUEST_TYPE_ID = createInteger("REQUEST_TYPE_ID");

    public final NumberPath<Integer> SERVICE_DESK_ID = createInteger("SERVICE_DESK_ID");

    public final DatePath UPDATED_DATE = createDateCol("UPDATED_DATE", Date.class).notNull().build();

    public final TimePath UPDATED_TIME = createTimeCol("UPDATED_TIME", Time.class).notNull().build();

    // a specific implementation
    public final EnumPath<Status> CURRENT_STATUS = createEnumCol("CURRENT_STATUS", Status.class).ofType(Types.JAVA_OBJECT).build();

    public final EnumPath<Status> DEFAULT_STATUS = createEnumCol("DEFAULT_STATUS", Status.class).build();


    public QEmailSettings() {
        super(QEmailSettings.class, "QEMAILSETTINGS_TABLE");

    }

    public enum Status {
        WAITING, WHINGING
    }
}
