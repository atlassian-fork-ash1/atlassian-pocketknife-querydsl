package com.atlassian.pocketknife.internal.querydsl;

import io.atlassian.fugue.Either;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.pocketknife.api.querydsl.EitherAwareDatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.util.OnRollback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import static io.atlassian.fugue.Either.left;
import static java.util.Objects.requireNonNull;

@Component
public class EitherAwareDatabaseAccessorImpl implements EitherAwareDatabaseAccessor {
    private static final Logger log = LoggerFactory.getLogger(EitherAwareDatabaseAccessorImpl.class);

    private final DatabaseAccessor databaseAccessor;

    @Autowired
    public EitherAwareDatabaseAccessorImpl(DatabaseAccessor databaseAccessor) {
        this.databaseAccessor = databaseAccessor;
    }

    @Override
    public <L, R> Either<L, R> runInNewEitherAwareTransaction(final Function<DatabaseConnection, Either<L, R>> function, final OnRollback onRollback) {
        // if we have a left that needs to trigger a rollback, record the value/reason here, so can be returned
        final AtomicReference<L> leftReference = new AtomicReference<>();
        try {
            return databaseAccessor.runInNewTransaction(databaseConnection -> {
                final Either<L, R> either = requireNonNull(function.apply(databaseConnection), "Callback result must not be null");
                either.left().forEach(l -> {
                    leftReference.set(l);
                    // if empty is returned, throw our runtime exception to trigger a rollback, but catch and return empty
                    throw new EitherAwareDatabaseAccessorTriggerRollbackException();
                });
                return either;
            }, onRollback);
        } catch (EitherAwareDatabaseAccessorTriggerRollbackException e) {
            if (log.isDebugEnabled()) {
                log.debug("Rollback was requested due to left '{}' being returned from either aware transaction", leftReference.get());
            }
            return left(leftReference.get());
        }
    }

    @Override
    public <L, R> Either<L, R> runInEitherAwareTransaction(final Function<DatabaseConnection, Either<L, R>> function, final OnRollback onRollback) {
        // if we have a left that needs to trigger a rollback, record the value/reason here, so can be returned
        final AtomicReference<L> leftReference = new AtomicReference<>();
        try {
            return databaseAccessor.runInTransaction(databaseConnection -> {
                final Either<L, R> either = requireNonNull(function.apply(databaseConnection), "Callback result must not be null");
                either.left().forEach(l -> {
                    leftReference.set(l);
                    // if empty is returned, throw our runtime exception to trigger a rollback, but catch and return empty
                    throw new EitherAwareDatabaseAccessorTriggerRollbackException();
                });
                return either;
            }, onRollback);
        } catch (EitherAwareDatabaseAccessorTriggerRollbackException e) {
            if (log.isDebugEnabled()) {
                log.debug("Rollback was requested due to left '{}' being returned from either aware transaction", leftReference.get());
            }
            return left(leftReference.get());
        }
    }

    private static class EitherAwareDatabaseAccessorTriggerRollbackException extends RuntimeException {
        EitherAwareDatabaseAccessorTriggerRollbackException() {
            super("RuntimeException to trigger a transaction rollback");
        }
    }
}
