package com.atlassian.pocketknife.internal.querydsl.cache;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Boolean.parseBoolean;
import static java.util.Objects.requireNonNull;

/**
 * This "stateful" class is need because some of components in PKQDSL are stateful for performance reasons
 * They cache certain decisions that would be too expensive to calculate on each DB call.
 *
 * However during testing of the apps the state fo the database can change dynamically and those decisions
 * need to be forgotten and re-calculated.  So this component is in charge of that.  It uses a
 * callback registration pattern to do this.
 */
@Component
public class PKQCacheClearerImpl implements PKQCacheClearer {
    private static final Logger log = LoggerFactory.getLogger(PKQCacheClearerImpl.class);
    private static final String PKQDSL_REACT_TO_CLEAR_CACHE = "pkqdsl.react.to.clear.cache";

    private final EventPublisher eventPublisher;
    private final List<Runnable> cacheClearingSideEffects;

    @Autowired
    public PKQCacheClearerImpl(@ComponentImport EventPublisher eventPublisher) {
        this.cacheClearingSideEffects = new ArrayList<>();
        this.eventPublisher = eventPublisher;
    }

    @PostConstruct
    private void postConstruction() {
        eventPublisher.register(this);
    }

    @PreDestroy
    void preDestroy() {
        eventPublisher.unregister(this);
    }

    @Override
    @EventListener
    public void onClearCache(Object event) {
        if (isClearCacheEvent(event)) {
            if (reactToClearCache()) {
                log.warn("Clearing the PKQDSL caches");
                clearAllCaches();
            }
        }
    }

    private boolean reactToClearCache() {
        return parseBoolean(System.getProperty(PKQDSL_REACT_TO_CLEAR_CACHE, "true"));
    }

    @VisibleForTesting
    boolean isClearCacheEvent(Object event) {
        //
        // we don't link against the  JIRA API in PKQDSL but we want to see this event.
        // This is how we can do it.
        //
        return "com.atlassian.jira.event.ClearCacheEvent".equals(event.getClass().getName());
    }

    /**
     * Called to register a callback that should happen when cache clearing should happen.
     *
     * @param runnable the caching clearing side effect
     */
    @Override
    public void registerCacheClearing(Runnable runnable) {
        this.cacheClearingSideEffects.add(requireNonNull(runnable));
    }

    @Override
    public void clearAllCaches() {
        cacheClearingSideEffects.forEach(Runnable::run);
    }
}
