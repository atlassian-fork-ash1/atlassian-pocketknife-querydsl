package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnectionConverter;
import com.atlassian.pocketknife.api.querydsl.util.OnRollback;
import com.atlassian.pocketknife.internal.querydsl.schema.DatabaseSchemaCreation;
import com.atlassian.sal.api.rdbms.TransactionalExecutor;
import com.atlassian.sal.api.rdbms.TransactionalExecutorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Function;

import static io.atlassian.fugue.Option.option;

@Component
public class DatabaseAccessorImpl implements DatabaseAccessor {

    private static final Logger log = LoggerFactory.getLogger(DatabaseAccessorImpl.class);
    private final DatabaseConnectionConverter databaseConnectionConverter;
    private final TransactionalExecutorFactory transactionalExecutorFactory;
    private final DatabaseSchemaCreation databaseSchemaCreation;

    @Autowired
    public DatabaseAccessorImpl(final DatabaseConnectionConverter databaseConnectionConverter,
                                final TransactionalExecutorFactory transactionalExecutorFactory,
                                final DatabaseSchemaCreation databaseSchemaCreation) {
        this.databaseConnectionConverter = databaseConnectionConverter;
        this.transactionalExecutorFactory = transactionalExecutorFactory;
        this.databaseSchemaCreation = databaseSchemaCreation;
    }

    @Override
    public <T> T runInNewTransaction(final Function<DatabaseConnection, T> function, final OnRollback onRollback) {
        return execute(function, false, true, onRollback);
    }

    @Override
    public <T> T runInTransaction(final Function<DatabaseConnection, T> function, final OnRollback onRollback) {
        return execute(function, false, false, onRollback);
    }

    /**
     * Offloads the execution to {@link TransactionalExecutor#execute(com.atlassian.sal.api.rdbms.ConnectionCallback)}
     *
     * @param function    to execute against a {@link DatabaseConnection}
     * @param readOnly    see {@link TransactionalExecutorFactory#createExecutor(boolean, boolean)}
     * @param requiresNew see {@link TransactionalExecutorFactory#createExecutor(boolean, boolean)}
     * @return return value of <code>function</code>
     */
    private <T> T execute(final Function<DatabaseConnection, T> function, final boolean readOnly, final boolean requiresNew, final OnRollback onRollback) {
        //
        // make sure the underlying schema is in place.  This decision will be
        // cached to ensure its fast.  We do this OUTSIDE the connection callback
        // so that we don't burn 2 connections in order to perform a schema creation
        // which is what would happen if this was done inside the connection filled
        // callback below
        //
        databaseSchemaCreation.prime();

        // use the SAL transaction support with its semantics
        TransactionalExecutor executor = transactionalExecutorFactory.createExecutor(readOnly, requiresNew);
        try {
            return executor.execute(jdbcConnection -> {
                final DatabaseConnection connection = databaseConnectionConverter.convertExternallyManaged(jdbcConnection);
                return function.apply(connection);
            });
        } catch (RuntimeException re) {
            // the TransactionalExecutor only knows about rollbacks, through exceptions, even the Either and Option aware
            // accessors need to throw an exception to signal a rollback. Just make sure to rethrow it unchanged afterwards
            try {
                option(onRollback).forEach(OnRollback::execute);
            } catch (Throwable ignored) {
                log.debug("Error throw from onRollback execution, will be logged, but ignored to ensure rethrown original exception", ignored);
            }
            // rethrow original error
            throw re;
        }
    }
}
