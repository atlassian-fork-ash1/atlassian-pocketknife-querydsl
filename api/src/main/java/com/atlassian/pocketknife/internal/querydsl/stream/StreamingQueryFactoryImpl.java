package com.atlassian.pocketknife.internal.querydsl.stream;

import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.pocketknife.api.querydsl.schema.DialectProvider;
import com.atlassian.pocketknife.api.querydsl.stream.ClosePromise;
import com.atlassian.pocketknife.api.querydsl.stream.CloseableIterable;
import com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory;
import com.atlassian.pocketknife.internal.querydsl.util.fp.Fp;
import com.google.common.collect.ImmutableList;
import com.mysema.commons.lang.CloseableIterator;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.StatementOptions;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * A factory to give back streamy results
 */
@Component
@ParametersAreNonnullByDefault
public class StreamingQueryFactoryImpl implements StreamingQueryFactory
{

    public static final int DEFAULT_FETCH_SIZE = 1000;


    @Override
    public <Q> CloseableIterable<Q> stream(final DatabaseConnection connection, final ClosePromise closePromise, final Supplier<SQLQuery<Q>> querySupplier)
    {
        return streamImpl(connection, closePromise, querySupplier);
    }

    @Override
    public <Q> CloseableIterable<Q> stream(final DatabaseConnection connection, final Supplier<SQLQuery<Q>> querySupplier)
    {
        return streamImpl(connection, ClosePromise.NOOP(), querySupplier);
    }

    private <Q> CloseableIterable<Q> streamImpl(final DatabaseConnection connection, final ClosePromise closeEffect, final Supplier<SQLQuery<Q>> function)
    {
        assertIsNotAutoCommit(connection);
        try
        {
            SQLQuery<Q> sqlQuery = function.get();
            sqlQuery = applyStreamingParameters(connection, sqlQuery);


            // make a streamy result
            final CloseableIterator<Q> iterator = sqlQuery.iterate();


            return new CloseableIterableImpl<>(iterator, Fp.identity(), closeEffect);
        }
        catch (RuntimeException rte)
        {
            closeEffect.close();
            throw rte;
        }
    }

    /**
     * On a streaming call we need to set fetch sizes and so on otherwise we are not getting the streamy-ness we think
     * we are. See https://jira.atlassian.com/browse/JRA-28591
     *
     * @param connection the connection in play
     * @param query the SQLQuery we are going to execute
     * @return the query with statement options
     */
    private <Q> SQLQuery<Q> applyStreamingParameters(DatabaseConnection connection, final SQLQuery<Q> query)
    {
        int fetchSize = DEFAULT_FETCH_SIZE;
        if (isMySQL(connection))
        {
            // JRA-28591 Use streaming mode on MySQL to stop OOMEs.  Nothing else seems to do any good. :(
            fetchSize = Integer.MIN_VALUE;
        }

        StatementOptions statementOptions = StatementOptions.builder().setFetchSize(fetchSize).build();
        query.setStatementOptions(statementOptions);
        return query;
    }

    private boolean isMySQL(final DatabaseConnection connection)
    {
        return connection.getDialectConfig().getDatabaseInfo().getSupportedDatabase() == DialectProvider.SupportedDatabase.MYSQL;
    }

    private void assertIsNotAutoCommit(final DatabaseConnection connection)
    {
        if (connection.isAutoCommit())
        {
            throw new IllegalStateException("The database connection for streamy operations MUST be NOT be in auto-commit mode");
        }
    }

    @Override
    public <Q, T> List<T> streamyMap(final DatabaseConnection connection, final StreamyMapClosure<Q, T> closure)
    {
        Function<DatabaseConnection, SQLQuery<Q>> queryFunction = closure.getQuery();
        CloseableIterable<Q> stream = stream(connection, () -> queryFunction.apply(connection));
        try (CloseableIterable<T> iterable = stream.map(closure.getMapFunction()))
        {
            return ImmutableList.copyOf(iterable);
        }
    }

    @Override
    public <Q, T> T streamyFold(final DatabaseConnection connection, final T initial, final StreamyFoldClosure<Q, T> closure)
    {
        Function<DatabaseConnection, SQLQuery<Q>> queryFunction = closure.getQuery();
        CloseableIterable<Q> stream = stream(connection, () -> queryFunction.apply(connection));
        return stream.foldLeft(initial, closure.getFoldFunction());
    }
}
