package com.atlassian.pocketknife.internal.querydsl.schema;

import com.atlassian.pocketknife.api.querydsl.schema.SchemaState;
import com.google.common.base.MoreObjects;
import com.querydsl.core.types.Path;
import com.querydsl.sql.RelationalPath;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.pocketknife.api.querydsl.schema.SchemaState.Presence.MISSING;

class SchemaStateImpl implements SchemaState {
    private final RelationalPath relationalPath;
    private final Presence tablePresence;
    private final Map<Path, Presence> columnState;
    private final Set<String> addedColumns;

    SchemaStateImpl(RelationalPath relationalPath, Presence tablePresence, Map<Path, Presence> columnState, Set<String> addedColumns) {
        this.relationalPath = relationalPath;
        this.tablePresence = tablePresence;
        this.columnState = columnState;
        this.addedColumns = addedColumns;
    }

    @Override
    public Presence getColumnState(Path<?> column) {
        return Optional.ofNullable(columnState.get(column)).orElse(MISSING);
    }

    @Override
    public RelationalPath getRelationalPath() {
        return relationalPath;
    }

    @Override
    public Presence getTableState() {
        return tablePresence;
    }

    @Override
    public Set<String> getAddedColumns() {
        return addedColumns;
    }

    @Override
    public Set<Path<?>> getMissingColumns() {
        Stream<Path<?>> pathStream = columnState
                .entrySet().stream()
                .filter(e -> e.getValue() == MISSING)
                .map(Map.Entry::getKey);
        return pathStream.collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("relationalPath", relationalPath)
                .add("tablePresence", tablePresence)
                .add("columnState", columnState)
                .add("addedColumns", addedColumns)
                .toString();
    }
}
