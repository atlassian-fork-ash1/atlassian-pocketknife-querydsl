package com.atlassian.pocketknife.internal.querydsl.testplugin.ao;

import net.java.ao.RawEntity;
import net.java.ao.schema.AutoIncrement;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.PrimaryKey;

public interface EntityWithId extends RawEntity<Long>
{
    public static final String ID = "ID";

    @AutoIncrement
    @NotNull
    @PrimaryKey(ID)
    public long getID();
}
