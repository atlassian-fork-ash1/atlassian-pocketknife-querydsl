package com.atlassian.pocketknife.internal.querydsl.testplugin.tables;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

/**
 */
public class QCustomerTable extends EnhancedRelationalPathBase<QCustomerTable>
{
    // Columns
    public final NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
    public final StringPath FIRST_NAME = createStringCol("FIRST_NAME").notNull().build();
    public final StringPath LAST_NAME = createStringCol("LAST_NAME").notNull().build();
    public final NumberPath<Long> CREATED_TIME = createLongCol("CREATED_TIME").notNull().build();
    public final NumberPath<Long> MODIFIED_TIME = createLongCol("MODIFIED_TIME").notNull().build();


    public QCustomerTable(final String tableName)
    {
        super(QCustomerTable.class, tableName);
    }

}
